import './App.css';
// import IncreaseButton from './Component/IncreaseButton';
import React, {Component} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import ProductName from './Component/ProductName';

export default class App extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <div className='App'>
        {/* <IncreaseButton /> */}
        <ProductName />
      </div>
    );
  }
}
