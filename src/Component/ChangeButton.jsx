import React, { Component } from "react";
import { Button } from "react-bootstrap";
export default class ChangeButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedState: this.props.isSelected,
    };
  }
  onSelect = () => {
    this.setState({
      selectedState: !this.state.selectedState,
    });

    this.props.onSelect(
      this.props.id,
      this.props.name,
      !this.state.selectedState
    );
  };
  render() {
    const { id, name } = this.props;
    return (
      <tr>
        <td>{id}</td>
        <td>{name}</td>
        <td>
          {this.state.selectedState ? (
            <Button onClick={this.onSelect} variant="success">
              select
            </Button>
          ) : (
            <Button onClick={this.onSelect} variant="danger">
              unselect
            </Button>
          )}
        </td>
      </tr>
    );
  }
}
