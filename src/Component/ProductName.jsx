import React from "react";
import { Button, Container, Table, Card, CardGroup } from "react-bootstrap";
import ChangeButton from "./ChangeButton";
import ChangeButtonOnCard from "./ChangeButtonOnCard";

var data = [
  {
    id: 1,
    name: "Coca Cola",
    isSelected: false,
  },
  {
    id: 2,
    name: "Pepsi",
    isSelected: false,
  },
  {
    id: 3,
    name: "Sting",
    isSelected: false,
  },
  {
    id: 4,
    name: "Milk",
    isSelected: true,
  },
];

class ProductName extends React.Component {
  constructor() {
    super();
    this.state = {
      isSelected: false,
      getData: [...data],
    };
  }

  onChangeCardButton = (id, name, isSelected) => {
    this.setState({
      getData: this.state.getData.map((obj) =>
        obj.id === id ? { id, name, isSelected } : obj
      ),
    });
  };

  render() {
    return (
      <div>
        <Container className="mt-5">
          <div className="myStyle">
            <h1 className="headingStyle mb-3">Welcome to My Shop</h1>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Product Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {data.map((obj, i) => (
                  <ChangeButton
                    key={i}
                    id={obj.id}
                    name={obj.name}
                    isSelected={obj.isSelected}
                    onSelect={this.onChangeCardButton}
                  />
                ))}
              </tbody>
            </Table>
          </div>

          <div className="mt-5 cardStyle">
            {this.state.getData.map((cardObj, j) => (
              <ChangeButtonOnCard
                key={j}
                id={cardObj.id}
                name={cardObj.name}
                isSelected={cardObj.isSelected}
              />
            ))}
          </div>
        </Container>
      </div>
    );
  }
}

export default ProductName;
