import React, { Component } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";

export default class ChangeButtonOnCard extends Component {
  render() {
    const { id, name, isSelected } = this.props;
    return (
      <Row style={{ display: "inline-block" }}>
        <Col>
          <Card className = "mt-5 mb-5" style = {{padding: "5px"}}>
            <Card.Img
              variant="top"
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTP2qB5zlQc8Vskr_iII-aNDEL_u5xBUZmrBA&usqp=CAU"
            />
            <Card.Body>
              <Card.Title>{id}</Card.Title>
              <Card.Text>
                {name} <br></br>
                {isSelected ? (
                  <Button variant="success" disabled>
                    select
                  </Button>
                ) : (
                  <Button variant="danger" disabled>
                    unselect
                  </Button>
                )}
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    );
  }
}
